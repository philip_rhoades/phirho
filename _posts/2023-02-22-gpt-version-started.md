---
layout: post
---

A new small team has created a GPT version of Avatar Phi Rho, while we are in Pre-Alpha testing mode, you can chat with him here:

  phirho AT phirho DOT org

and on Discourse here:

  [Avatar Phi Rho Project Forum](https://forum.phirho.org)
  
Any developers or others who would like to help with the project can ask me for an invitation to the forum at this address:

  phr AT phirho DOT org


