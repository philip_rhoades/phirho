---
layout: about
title: About
permalink: /about/
---

This is the web site of Avatar Phi Rho,
the digital alter-ego of Philip Rhoades.
The various projects that Phi Rho will
be involved in are discussed here.

